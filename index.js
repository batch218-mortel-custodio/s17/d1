// Functions in JS are lines/block of codes that tell our device/application to perform a certian task when called/invoked.
// They are also used to prevent repeating lines/block of codes that perform the same task/function.

//Funtion Declaration - function statement defines a function with parameters

function printName(){ // code block
	console.log("My name is Johann"); //function statement
};

printName(); // Function Invocation

//[HOISTING] - you can call a function even though there are no declarations on the previous codes. but it  has to be present (after)

declaredFunction();	

function declaredFunction(){
	console.log("Elo whoreld!");
};

// Function expression - can also be stored in a variable. cannot be hoisted!

let variableFunction = function(){
	console.log("henlo agin!");
};

variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side");
};

funcExpression();
//funcName(); you cannot canot call this one

console.log("-----------");
console.log("[Reassigning Functions]");

declaredFunction();

declaredFunction = function(){
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression");
};

funcExpression();

//Constant function

const constantFunction = function(){
	console.log("Initialized  with const.");
};


constantFunction();

//function with const keyword cannot be reassigned.
// scope is the accessibility / visibility of a variables in the code

// Function Scoping

// java variables has 3 tyhpes of scope
//		local /block scope, globals scope, function scope

console.log("-----------");
console.log("[Function Scoping]");

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

/*console.log(localVar);*/



let globalVar = "MrWW";
console.log(globalVar);

{
	console.log(globalVar);
}



//[Func scoping]
// Variables defined inside a function ARE NOT accessible/visible outside the function
// Variables declared with var, let, and const are quite similar when delared inside a function


function showNames(){
	var functionVar = "Joe";
	const functionConst ="John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();


// Nested Functions

console.log("-----------");

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction();
	console.log(nestedFunction);
}

myNewFunction();

// Global Scoped Variable

let globalName = "Zuitt";

function myNewFunction2(){
		let nameInside = "Renz";
		console.log(globalName);
		console.log(nameInside);
}

myNewFunction2();
/*console.log(nameInside);*/


// alert() - allows us to show a small window at the top of our browser page to show info to our users.

//alert("you cannot");

function showSampleAlert(){ //function name must contain a verb
	alert("Hi crush!")
}

showSampleAlert();

//alert messages inside a function will only execute whenever it is called

console.log("will only log in the console when the alert is dismissed");


// Notes on the use of alert () - show only an alert for short dialogs/message to the user and has only a character limit
//do not overuse alert because  the program/js has to wait for it to be dismissed before continuing


// [Prompt] - prompt() allow us to show small window at the top of the browser to gather user input

// let samplePrompt = prompt('Enter your pull name');
// //console.log(samplePrompt);

// console.log(typeof samplePrompt); // even numbers are considered string here

// console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, "+ firstName + " "+ lastName +"!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();


//function name must start with a verb. avoid generic names to avoid confusion within our code. avoid pointless and inappropriate fnction names like foo, bar.....
function getCourses(){
	let courses = ["Sci 101", "Mat 101", "Eng 101"];
	console.log(courses);
}

getCourses();



function getCourses(){
	let courses = ["Sci 101", "Mat 101", "Eng 101"];
	console.log(courses);
}

getCourses();


function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}

displayCarInfo();

